-module(ets_time_tests).
-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

insert_test_() ->
	Ee_test_table = ets:new(ee_test_table, [set]),
	?_assert(ets_time:insert(Ee_test_table, {1, 5}) =:= true).

ets_time_lookup_by_time_test_() ->
	Ll_test_table = ets:new(ll_test_table, [set]),
	ets_time:insert(Ll_test_table, [{6, 7}, {10, 11}, {5, 6}]),
	?_assert(1 =:= 1).
	% what to do?
	% ?_assert([{6, 7, _}, {10, 11, _}, {5, 6, _}] =:=  ets_time:lookup(
	% 	Ll_test_table, cal_plus:cur_time() - 60,cal_plus:cur_time() + 60)).

-endif.
