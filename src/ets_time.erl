-module(ets_time).
-export ([insert/2, lookup/3]).
% -export ([insert/2]).

insert(ETS, {Key, Value}) ->
	ets:insert(ETS, {Key, Value, cal_plus:cur_time()});

insert(_ETS, []) ->
	true;

insert(ETS, [{Key, Value} | Tail]) ->
	ets:insert(ETS, {Key, Value, cal_plus:cur_time()}),
	insert(ETS, Tail).

lookup(ETS, Start_time = {{_, _, _}, {_, _, _}}, End_time = {{_, _, _}, {_, _, _}}) ->
	lookup(ETS, cal_plus:to_sec(Start_time), cal_plus:to_sec(End_time));

lookup(ETS, Start_time, End_time) ->
	ets:select(ETS, [_Match = {{'$1', '$2', '$3'},
					 _Guards = [{'=<', Start_time, '$3'}, {'=<', '$3', End_time}],
					 _Res = ['$_']}]).