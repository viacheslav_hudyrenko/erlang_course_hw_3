-module(cal_plus).
-export([cur_time/0, to_sec/1]).

cur_time() ->
	calendar:datetime_to_gregorian_seconds(calendar:universal_time()).

to_sec({{_, _, _}, {_, _, _}} = DateTime) ->
	calendar:datetime_to_gregorian_seconds(DateTime).